import { Component, OnInit } from '@angular/core';
import { Hero } from '../hero';
import { ActivatedRoute } from '@angular/router';
import { HeroService } from '../hero.service';
import { Router } from '@angular/router';
import { HeroesComponent } from '../heroes/heroes.component';
import { NotificationService } from '../notification.service';
import { trigger, state, animate, transition, style } from '@angular/animations';

@Component({
	selector: 'app-hero-detail',
	templateUrl: './hero-detail.component.html',
	styleUrls: ['./hero-detail.component.css'],
	animations: [
		trigger('fadeInAnimation', [
			transition(':enter', [
				style({ opacity: 0 }),
				animate('0.3s', style({ opacity: 1 }))
			])
		])
	]
})
export class HeroDetailComponent implements OnInit {
	private routeSubscription;

	hero: Hero;
	heroes: Hero[];
	model: string;
	id: number;
	disableEdit = true;
	loading: boolean = false;
	dataBase = 'http://localhost:3000';

	constructor(private route: ActivatedRoute, 
		private heroService: HeroService, 
		private router: Router, 
		private notif: NotificationService,
		private heroesComponent: HeroesComponent	
	) { }

	ngOnInit(): void {
		this.routeSubscription = this.route.params.subscribe(params => {
			this.id = +params.id;
			this.model = this.route.parent.snapshot.paramMap.get('model');
			this.getHero();
		});
	}

	ngOnDestroy(): void {
		this.routeSubscription.unsubscribe();
	}

	getHero(): void {
		this.loading = true;
		this.heroService.getHero(this.id, `${this.dataBase}/${this.model}`)
			.subscribe(hero => {
				this.hero = hero
				this.loading = false;
			}, (error) => {
				this.router.navigate(['/unknownpage']);
			});
	}

	goBack(): void {
		this.router.navigate([`/${this.model}`]);
	}

	onNotify(message): void {
		this.save(message);
	}

	save(character: Object): void {
		this.loading = true;
		this.heroService.updateHero(character as Hero, `${this.dataBase}/${this.model}/${this.id}`)
			.subscribe(() => {
				this.loading = false;
				if (JSON.stringify(character) == JSON.stringify(this.hero)) {
					this.notif.show("No changes were made", this.continueFunc.bind(this));
				}
				else {
					this.notif.show(`Updated anyone id=${this.hero.id}`, this.continueFunc.bind(this))
					this.heroesComponent.getHeroes();
				}
				this.goBack();
			});
	}

	editToggle(): void {
		this.disableEdit = !this.disableEdit;
	}

	continueFunc(): void {
		this.goBack();
	}
}
