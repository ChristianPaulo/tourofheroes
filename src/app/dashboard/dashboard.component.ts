import { Component, OnInit } from '@angular/core';
import { Hero } from '../hero';
import { HeroService } from '../hero.service';
import { Router } from '@angular/router';
import { trigger, state, animate, transition, style } from '@angular/animations';
import { forEach } from '@angular/router/src/utils/collection';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  animations: [
		trigger('fadeInAnimation', [
			transition(':enter', [
				style({ opacity: 0 }),
				animate('0.5s', style({ opacity: 1 }))
			])
		])
	]
})
export class DashboardComponent implements OnInit {
  heroes: Hero[][] = [];
  loading: boolean = false;
  dataBase = 'http://localhost:3000';
  types = [
    { model: 'heroes', id: 1 },
    { model: 'villains', id: 2 }
  ];

  constructor(private heroService: HeroService, private router: Router) { }

  ngOnInit() {
    this.types.forEach((type, index) => this.getHeroes(type.model, index));
  }

  getHeroes(model: string, x: number): void {
    this.loading = true;
    this.heroService.getHeroes(`${this.dataBase}/${model}`)
      .subscribe(heroes => {
        this.heroes[x] = heroes.slice(1, 5);
        this.loading = false;
      }, (error) => {
        this.loading = false;
        this.router.navigate(['/unknownpage']);
      });
  }
}