import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const heroes = [
      { id: 11, name: 'Mr. Nice', codename: 'catdog' },
      { id: 12, name: 'Narco', codename: '' },
      { id: 13, name: 'Bombasto', codename: '' },
      { id: 14, name: 'Celeritas', codename: '' },
      { id: 15, name: 'Magneta', codename: '' },
      { id: 16, name: 'RubberMan', codename: '' },
      { id: 17, name: 'Dynama', codename: '' },
      { id: 18, name: 'Dr IQ', codename: '' },
      { id: 19, name: 'Magma', codename: '' },
      { id: 20, name: 'Tornado', codename: '' }
    ];
    const villains = [
      { id: 11, name: 'Mr. Bad', codename: '', minion: 'bat' },
      { id: 12, name: 'Narcotics', codename: 'weedman', minion: 'zombie' },
      { id: 13, name: 'Bombadass', codename: '', minion: 'human' },
      { id: 14, name: 'Celery', codename: '', minion: 'robot' },
      { id: 15, name: 'Professor Y', codename: '', minion: 'zombie' },
      { id: 16, name: 'TireMan', codename: '', minion: 'zombie' },
      { id: 17, name: 'Dirty Bubble', codename: '', minion: 'robot' },
      { id: 18, name: 'Dr EQ', codename: '', minion: 'bat' },
      { id: 19, name: 'Chillman', codename: '', minion: 'robot' },
      { id: 20, name: 'Oh no', codename: '', minion: 'human' }
    ];
    return { heroes, villains };
  }
}