export class Villain {
    id: number;
    name: string;
    codename: string;
    minion: String;
  }