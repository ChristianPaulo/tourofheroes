import { NgModule } 			from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HeroesComponent } 		from './heroes/heroes.component';
import { DashboardComponent }   from './dashboard/dashboard.component';
import { HeroDetailComponent }  from './hero-detail/hero-detail.component';
import { PageErrorComponent } 	from './page-error/page-error.component';

const routes: Routes = [
	{ path: '', redirectTo: '/dashboard', pathMatch: 'full' },
	{ path: 'dashboard', component: DashboardComponent },
	{ path: 'unknownpage', component: PageErrorComponent },
	{ path: ':model', component: HeroesComponent,
	  	children: [
					{ path: ':id', component: HeroDetailComponent }
		]
	},
	{ path: '**', component: PageErrorComponent }
];

@NgModule({
	imports: [ RouterModule.forRoot(routes) ],
  	exports: [ RouterModule ]
})
export class AppRoutingModule {}

