import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class NotificationService {
  onShow = new Subject<any>();
  onHide = new Subject<any>();

  hide() {
    this.onHide.next();
  }

  show(message: string, okFunction?: Function, cancelFunction?: Function) {
    this.onShow.next({ message: message, okFunction: okFunction, cancelFunction: cancelFunction });
  }
}
