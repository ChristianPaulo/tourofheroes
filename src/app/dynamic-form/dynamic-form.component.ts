import { Component, EventEmitter, Input, Output, OnChanges } from '@angular/core';
import { FormGroup, ValidationErrors } from '@angular/forms';
import { QuestionBase } from '../question-base';
import { QuestionControlService } from '../question-control.service';
import { QuestionService } from '../question.service';
import { NotificationService } from '../notification.service';

@Component({
  selector: 'app-dynamic-form',
  templateUrl: './dynamic-form.component.html',
  styleUrls: ['./dynamic-form.component.css'],
  providers: [QuestionService, QuestionControlService]
})
export class DynamicFormComponent implements OnChanges {
  questions: QuestionBase<any>[] = [];
  @Input() hero: Object;
  @Input() model: string;
  @Input() formDisabler: boolean;
  @Output() notify: EventEmitter<any[]> = new EventEmitter<any[]>();
  form: FormGroup;
  submitted = false;

  constructor(private qcs: QuestionControlService, private service: QuestionService, private notif: NotificationService) { }

  ngOnChanges() {
    this.questions = this.service.getQuestions(this.model);
    if (this.hero) {
      this.setFormValues();
    }

    this.form = this.qcs.toFormGroup(this.questions);

    this.submitted = false;

    if (this.formDisabler) {
      this.form.disable();
    }
    else {
      this.form.enable();
    }
  }

  onSubmit() {
    this.submitted = true;
    if (!this.form.valid) {
      this.notif.show(this.getAllErrors());
      return;
    }
    this.notif.show("Are you sure?", this.okFunc.bind(this), Function);
  }

  getAllErrors(): string{
    var allErrors
    Object.keys(this.form.controls).forEach(key => {

      const controlErrors: ValidationErrors = this.form.get(key).errors;
      if (controlErrors != null) {
        Object.keys(controlErrors).forEach(keyError => {
          allErrors += key + ' ' + keyError + ' error \n';
        });
      }
    });
    return allErrors;
  }

  setFormValues() {
    Object.keys(this.questions).forEach(i => {
			this.questions[i].value = this.hero[this.questions[i].key];
    });
  }

  okFunc() {
		let formValue = this.form.value;
		if (this.hero) {
			formValue = Object.assign({ 'id': this.hero['id'] }, this.form.value);
		}
    this.notify.emit(formValue);
  }
}