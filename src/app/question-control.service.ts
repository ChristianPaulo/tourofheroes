import { Injectable } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { QuestionBase } from './question-base';

@Injectable()
export class QuestionControlService {
  constructor() { }

  toFormGroup(questions: QuestionBase<any>[]) {
    let group: any = {};

    questions.forEach(question => {
      group[question.key] = question.required ? new FormControl(question.value || '', [Validators.required, Validators.pattern('^[a-zA-Z0-9-_ ]+$')])
        : question.patternletters ? new FormControl(question.value || '', [Validators.pattern('^[A-z]+$')]) : new FormControl(question.value || '');

    });
    return new FormGroup(group);
  }
}