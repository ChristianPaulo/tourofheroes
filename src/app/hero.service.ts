import { Injectable } from '@angular/core';
import { Hero } from './hero';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { MessageService } from './message.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { NotificationService } from './notification.service';

const httpOptions = {
	headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class HeroService {

	getHeroes(path: string, param1?: string, param2?: string, param3?: string, param4?: string): Observable<Hero[]> {
		//for sort and search with page
		if (param4 != undefined) {
			param1 = `name_like=${param1}`;
			param2 = `&_sort=${param2}`;
			param3 = `&_order=${param3}`;
			param4 = `&_page=${param4}`;
		}
		//for sort and search page = 1
		else if (param3 != undefined) {
			param1 = `name_like=${param1}`;
			param2 = `&_sort=${param2}`;
			param3 = `&_order=${param3}`;
			param4 = '&_page=1';
		}
		//for sort
		else if (param2 != undefined) {
			param1 = `&_sort=${param1}`;
			param2 = `&_order=${param2}`;
			param3 = '';
			param4 = '&_page=1';
		}
		//for search
		else if (param1 != undefined) {
			if (param1) {
				param1 = `name_like=${param1}`;
				param2 = '';
				param3 = '';
				param4 = '&_page=1';
			}
			else {
				return of([])
			}
		}
		else {
			param1 = '';
			param2 = '';
			param3 = '';
			param4 = '&_page=1';
		}
		// console.log(`${path}?${param1}${param2}${param3}${param4}`);
		return this.http.get<Hero[]>(`${path}?${param1}${param2}${param3}${param4}`);
	}

	getHero(id: number, path: string): Observable<Hero> {
		const url = `${path}/${id}`;
		return this.http.get<Hero>(url);
	}

	updateHero(hero: Hero, path: string): Observable<any> {
		if (hero.id != null) {
			return this.http.put(path, hero, httpOptions);
		}
		else {
			return this.http.post<Hero>(path, hero, httpOptions);
		}
	}

	deleteHero(hero: Hero | number, path: string): Observable<Hero> {
		const id = typeof hero === 'number' ? hero : hero.id;
		const url = `${path}/${id}`;
		return this.http.delete<Hero>(url, httpOptions);
	}

	searchHeroes(term: string, path: string): Observable<Hero[]> {
		
		return this.http.get<Hero[]>(`${path}?name_like=${term}`);
	}

	constructor(
		private http: HttpClient,
		private messageService: MessageService,
		private router: Router,
		private notificationService: NotificationService) { }

	private log(message: string) {
		this.messageService.add(message);
		this.notificationService
	}
}
