import { Injectable } from '@angular/core';
import { QuestionBase } from './question-base';
import { TextboxQuestion } from './question-textbox';
import { DropdownQuestion } from './question-dropdown';

@Injectable()
export class QuestionService {
  getQuestions(str: string) {
    if (str == 'heroes') {
      let heroes: QuestionBase<any>[] = [

        new TextboxQuestion({
          key: 'name',
          label: 'Name',
          required: true,
          order: 1
        }),

        new TextboxQuestion({
          key: 'codename',
          label: 'Codename',
          order: 2,
          patternletters: true
        })
      ];
      return heroes.sort((a, b) => a.order - b.order);
    }
    if (str == 'villains') {
      let villains: QuestionBase<any>[] = [

        new TextboxQuestion({
          key: 'name',
          label: 'Name',
          required: true,
          order: 1
        }),

        new TextboxQuestion({
          key: 'codename',
          label: 'Codename',
          order: 2,
          patternletters: true
        }),

        new DropdownQuestion({
          key: 'minion',
          label: 'Minion Type',
          options: [
            { key: 'zombie', value: 'Zombie' },
            { key: 'robot', value: 'Robot' },
            { key: 'bat', value: 'Bat' },
            { key: 'human', value: 'Human' }
          ],
          order: 3,
          value: 'zombie'
        })
      ];
      return villains.sort((a, b) => a.order - b.order);
    }
  }
}