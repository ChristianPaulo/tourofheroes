import { Component } from '@angular/core';
import { NotificationService } from '../notification.service'
import { trigger, style, animate, transition } from '@angular/animations';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css'],
  animations: [
    trigger(
      'enterAnimation', [
        transition(':enter', [
          style({transform: 'translateY(-20%)', opacity: 0}),
          animate('250ms', style({transform: 'translateY(0)', opacity: 1}))
        ]),
        transition(':leave', [
          style({transform: 'translateY(0)', opacity: 1}),
          animate('250ms', style({transform: 'translateY(-20%)', opacity: 0}))
        ])
      ]
    )
  ]
})
export class NotificationComponent {
  active: boolean = false;
  message: string;
  okFunction: Function;
  cancelFunction: Function;

  constructor(private notificationService: NotificationService) {
    this.notificationService.onHide.subscribe(() => { this.hide() })
    this.notificationService.onShow.subscribe(obj => { this.show(obj) })
  }

  onOk() {
    this.okFunction();
  }

  onCancel() {
    this.cancelFunction();
  }

  hide() {
    this.active = false;
  }

  show(obj) {
    this.active = true;
    this.message = obj.message;
    this.okFunction = obj.okFunction;
    this.cancelFunction = obj.cancelFunction;
  }
}
