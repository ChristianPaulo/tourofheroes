import { Component, OnInit, Input } from '@angular/core';
import { Hero } from '../hero';
import { HeroService } from '../hero.service'
import { ActivatedRoute, Router } from '@angular/router';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { Subject } from 'rxjs/Subject';
import { NotificationService } from '../notification.service';
import { trigger, state, animate, transition, style } from '@angular/animations';

@Component({
	selector: 'app-heroes',
	templateUrl: './heroes.component.html',
	styleUrls: ['./heroes.component.css'],
	animations: [
		trigger('fadeInAnimation', [
			transition(':enter', [
				style({ opacity: 0 }),
				animate('0.2s', style({ opacity: 1 }))
			]),
			transition(':leave', [
				style({ opacity: 1 }),
				animate('0.1s', style({ opacity: 0 }))
			])
		])
	]
})
export class HeroesComponent implements OnInit {
	private routeSubscription;

	heroes: Hero[] = [];
	hero: Hero;
	model: string;
	loading: boolean = false;
	dataBase: string = 'http://localhost:3000';
	pageLength: number;
	searchTerm: string;
	sortKey: string;
	order: string;
	pageNumber: number;

	constructor(private heroService: HeroService, private route: ActivatedRoute,
		private router: Router, private notif: NotificationService) { }

	ngOnInit(): void {
		this.routeSubscription = this.route.params.subscribe((params) => {
			this.model = this.route.snapshot.paramMap.get('model');
			this.pageLength = 0;
			this.searchTerm = '';
			this.sortKey = '';
			this.order = '';
			this.pageNumber = 1;
			this.getHeroes();
		});
	}

	ngOnDestroy(): void {
		this.routeSubscription.unsubscribe();
	}

	onNotify(message): void {
		this.add(message);
	}

	getHeroes(): void {
		this.heroService.getHeroes(`${this.dataBase}/${this.model}`, '', '', '', '')
			.subscribe((heroes) => {
				this.pageLength = Math.ceil(heroes.length/10);
			});
		this.loading = true;
		this.pageNumber = 1;
		this.heroService.getHeroes(`${this.dataBase}/${this.model}`)
			.subscribe(heroes => {
				this.loading = false;
				this.heroes = heroes;
			}, (error) => {
				this.router.navigate(['/unknownpage']);
			});
	}

	add(character: Object): void {
		this.loading = true;
		this.heroService.updateHero(character as Hero, `${this.dataBase}/${this.model}`)
			.subscribe(hero => {
				this.loading = false;
				this.getHeroes();
				this.notif.show(`added anyone id=${hero.id}`);
			});
	}

	delete(hero: Hero): void {
		this.loading = true;
		const heroCopy = hero;
		this.heroService.deleteHero(hero, `${this.dataBase}/${this.model}`).subscribe(() => {
			this.loading = false;
			this.getHeroes();
			this.notif.show(`deleted anyone id=${heroCopy.id}`);
		});
	}

	sortIt(searched: string, sort: string, order: string): void {
		this.loading = true;
		this.pageNumber = 1;
		this.searchTerm = searched;
		this.sortKey = sort;
		this.order = order;
		this.heroService.getHeroes(`${this.dataBase}/${this.model}`, searched, sort, order)
			.subscribe(heroes => {
				this.loading = false;
				this.heroes = heroes;
			}, (error) => {
				this.router.navigate(['/unknownpage']);
			});
	}

	setPage(pageNumber: number): void {
		this.loading = true;
		this.heroService.getHeroes(`${this.dataBase}/${this.model}`, this.searchTerm, this.sortKey, this.order, pageNumber.toString()).subscribe((heroes) => {
			this.loading = false;
			this.heroes = heroes;
		});
	}

	search(term: string): void {
		if (!term.match('^[a-zA-Z0-9-_ ]+$') || term == '') {
			this.searchTerm = '';
			this.getHeroes();
			return;
		}
		this.heroService.getHeroes(`${this.dataBase}/${this.model}`, term, '', '', ' ')
			.subscribe((heroes) => {
				this.pageLength = Math.ceil(heroes.length/10);
			});
		this.loading = true;
		this.pageNumber = 1;
		this.searchTerm = term;
		this.heroService.getHeroes(`${this.dataBase}/${this.model}`, term)
			.subscribe((heroes) => {
				this.loading = false;
				this.heroes = heroes;
				debounceTime(300),
					distinctUntilChanged()
			});
	}
}
